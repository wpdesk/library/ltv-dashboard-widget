[![pipeline status](https://gitlab.com/wpdesk/library/ltv-dashboard-widget/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/library/ltv-dashboard-widget/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/library/ltv-dashboard-widget/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/library/ltv-dashboard-widget/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/library/ltv-dashboard-widget/v/stable)](https://packagist.org/packages/wpdesk/library/ltv-dashboard-widget) 
[![Total Downloads](https://poser.pugx.org/wpdesk/library/ltv-dashboard-widget/downloads)](https://packagist.org/packages/wpdesk/library/ltv-dashboard-widget) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/library/ltv-dashboard-widget/v/unstable)](https://packagist.org/packages/wpdesk/library/ltv-dashboard-widget) 
[![License](https://poser.pugx.org/wpdesk/library/ltv-dashboard-widget/license)](https://packagist.org/packages/wpdesk/library/ltv-dashboard-widget) 

# ltv-dashboard-widget

A simple, yet very useful library for WordPress plugins allowing to display WP Desk dashboard widget.

## Requirements

PHP 7.2 or later.

## Installation via Composer

In order to install the bindings via [Composer](http://getcomposer.org/) run the following command:

```bash
composer require wpdesk/library/ltv-dashboard-widget
```

Next, use the Composer's [autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading) to use them:

```php
require_once 'vendor/autoload.php';
```

## Manual installation

If you prefer not to use the Composer you can also [download the latest library release](https://gitlab.com/wpdesk/library/ltv-dashboard-widget/-/jobs/artifacts/master/download?job=library). 

```php
require_once('/path/to/wp-desk/ltv-dashboard-widget/src/DashboardWidget.php');
```

## Getting Started

### Notices usage example

```php
(new WPDesk\Dashboard\DashboardWidget())->hooks();
```
